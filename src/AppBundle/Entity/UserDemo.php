<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * UserDemo
 *
 * @ORM\Table(name="user_demo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserDemoRepository")
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="Un compte existe déjà avec cette adresse email."
 * )
 */
class UserDemo implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     * @Assert\Length(
     *      min = 0,
     *      minMessage = "Vous devez mettre un pseudo !"
     * )
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email(
     *     message = "Vous devez entrer une adresse email valide",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "Le mot de passe doit contenir au moins 6 caractères"
     * )
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validityToken", type="datetime", nullable=true)
     */
    private $validityToken;

    /**
     * @var int
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", nullable=true)
     */
    private $role;


    //---------------------------------------
    // créer token
    //---------------------------------------

    public function createToken() {
        $this->token = uniqid('compte',true);
    }

    //---------------------------------------
    // créer validité du token
    //---------------------------------------

    public function createValidityToken() {
        $validityToken = new \DateTime();
        $validityToken = date_add($validityToken, date_interval_create_from_date_string('+3 hours'));
        $this->validityToken = $validityToken;
    }

    //---------------------------------------
    // créer methode de la UserInterface
    //---------------------------------------

    public function eraseCredentials() {}

    public function getSalt() {}

    public function getRoles() {

            if ($this->role == 'ROLE_ADMIN' ) {

                $role = ['ROLE_ADMIN'];

            } else {
                $role = ['ROLE_USER'];
            }

        return $role;
    }


    /**
     * @var string
     *
     * @Assert\EqualTo(
     *     propertyPath = "password",
     *     message = "Les deux mot de passe doivent être identiques"
     * )
     * @Assert\Length(
     *      min = 0,
     *      minMessage = "Le mot de passe doit contenir au moins 6 caractères"
     * )
     */
    public $confirm_password;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return UserDemo
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return UserDemo
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return UserDemo
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return UserDemo
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set validityToken
     *
     * @param \DateTime $validityToken
     *
     * @return UserDemo
     */
    public function setValidityToken($validityToken)
    {
        $this->validityToken = $validityToken;

        return $this;
    }

    /**
     * Get validityToken
     *
     * @return \DateTime
     */
    public function getValidityToken()
    {
        return $this->validityToken;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return UserDemo
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return UserDemo
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }


}
