<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use AppBundle\Entity\UserDemo;
use AppBundle\Entity\Recipe;
use AppBundle\Form\UserDemoType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class SecurityController extends Controller
{
    /**
     * @Method({"GET", "POST"})
     *
     * @Route("/inscription", name="inscription_page")
     */
    public function inscriptionAction(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        // $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);
        $user = new UserDemo();
        $form = $this->createForm(UserDemoType::class, $user);

        // Set de l'objet + exceptions

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->createToken();
            $user->createValidityToken();

            $password = $user->getPassword();
            $hash = $encoder->encodePassword($user, $password);
            $user->setPassword($hash);
            $user->setRole('ROLE_USER');

            $to = $user->getEmail();
            $subject = "activation compte demo Eric TASCA";
            $from = "erictasca.contact@gmail.com";
            $headers = 'From: erictasca.contact@gmail.com' . "\r\n" .
            'Reply-To: erictasca.contact@gmail.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            $message ="Bonjour, cliquez sur le lien ci dessous pour valider votre compte :
            http://www.eric-tasca.fr/demo-recette/web/validation-inscription/" . $user->getToken() . "
            ";
            mail($to, $subject, $message, $headers);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('user_success');
        }
        // replace this example code with whatever you need
        return $this->render('default/inscription.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     *
     * @Route("/validation-inscription/{token}", name="user_activate")
     */
    public function tokenUserAction($token, Request $request, ObjectManager $manager) {

        $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);

        if($user = $userRepository->findOneByToken($token)) {

            if ($user->getToken() == NULL) {
                return $this->redirectToRoute('homepage');
            }

            $date = new \DateTime();

            if ($date > $user->getValidityToken()) {
                $manager->remove($user);
                $manager->flush();
                return $this->redirectToRoute('inscription_page');
            }

            if ($user->getActive() === NULL) {

                $user->setActive(1);
                $user->setToken(NULL);
                $user->setValidityToken(NULL);
                // $userRepository->updateToken($token);


                $manager->persist($user);
                $manager->flush();

                return $this->render('default/validation-inscription.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
                ]);
            }
            if ($user->getActive() === 1) {
                return $this->redirectToRoute('homepage');
            }

        } else {
            return $this->redirectToRoute('homepage');
        };
        // var_dump($user->getActive());exit();

    }

    /**
     * @Method({"GET", "POST"})
     *
     * @Route("/connexion", name="connexion_page")
     *
     */
    public function connexionAction(Request $request, AuthenticationUtils $authenticationUtils) {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/connexion.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/deconnexion", name="deconnexion_page")
     */
    public function deconnexionAction() {

    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/confirmation-suppression-compte", name="confirm_delete_account")
     */
    public function confirmDeleteAccountAction(ObjectManager $manager) {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/confirmDeleteAccount.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/supprimer-compte", name="delete_account")
     */
    public function deleteAccountAction(ObjectManager $manager) {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }

        $recipeRepository = $this->getDoctrine()->getRepository(Recipe::class);

        $user = $this->getUser();

        $recipes = $recipeRepository->findByUser($user);

        foreach($recipes as $recipe) {
            $manager->remove($recipe);
        }

        //Destruction de toutes les variables de session
        session_unset();

        //Destruction de la session elle-même
        session_destroy();
        $manager->remove($user);
        $manager->flush();

        return $this->redirectToRoute('homepage');
    }
}
