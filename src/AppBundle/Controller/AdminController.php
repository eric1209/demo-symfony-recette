<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use AppBundle\Entity\UserDemo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;



class AdminController extends Controller
{

    /**
     *
     * @Route("/admin", name="home_admin")
     *
     */
    public function adminAction(Request $request) {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

            return $this->redirectToRoute('homepage');

        }

        return $this->render('admin/accueil.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);

    }

    /**
     *
     * @Route("/admin/utilisateurs", name="show_users")
     *
     */
    public function showUsersAction(Request $request) {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);
        $users = $userRepository->findAll();

        return $this->render('admin/users.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'users' => $users
        ]);

     }

    /**
     *
     * @Route("/admin/supprimer-utilisateur-confirm/{id}", name="delete_user_confirm")
     *
     */
    public function confirmDeleteUserAction(UserDemo $user, Request $request) {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('admin/confirmDeleteUser.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'user' => $user
        ]);

    }

    /**
     *
     * @Route("/admin/supprimer-utilisateur/{id}", name="delete_user")
     *
     */
    public function deleteUserAction(UserDemo $user, ObjectManager $manager) {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        $manager->remove($user);
        $manager->flush();

        return $this->redirectToRoute('show_users');

    }

}
