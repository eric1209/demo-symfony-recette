<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserDemo;
use AppBundle\Form\UserDemoType;
use AppBundle\Entity\Recipe;
use AppBundle\Form\RecipeType;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\DateTime;



class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/finalisation-inscription", name="user_success")
     */
    public function succesUserAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/userSucces.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/creer-recette", name="create_recipe")
     * @Route("/recette/{id}/modifier", name="update_recipe")
     */
    public function formRecipeAction(Recipe $recipe = NULL, Request $request, ObjectManager $manager, AuthenticationUtils $authenticationUtils)
    {

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }

        $user = $this->getUser();

        if ($recipe == NULL) {
            $isExist = 0;
            $recipe = new Recipe();
            $oldTitle = NULL;
            $oldIngredients = NULL;
            $oldProcess = NULL;
            $form = $this->createForm(RecipeType::class, $recipe);
        } else {
            $isExist = 1;
            $oldImage = $recipe->getImageFile();
            $recipe->setImageFile(NULL);
            $oldTitle = $recipe->getTitle();
            $oldIngredients = $recipe->getIngredients();
            $oldProcess = $recipe->getProcess();

            $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);


            if (!$userRepository->findOneById($recipe->getUser())) {
                return $this->redirectToRoute('recipes_list');
            } else {
                $author = $userRepository->findOneById($recipe->getUser());
            }

            if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
                if ($author->getId() != $user->getId()) {
                    return $this->redirectToRoute('recipes_list');
                }
            }

            $recipeNew = new Recipe();

            $form = $this->createFormBuilder()
                ->add('title')
                ->add('ingredients', TextareaType::class)
                ->add('process', TextareaType::class)
                ->add('save', SubmitType::class)
                ->getForm();
        }
        
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            if ($isExist == 1) {
                $recipeRepository = $this->getDoctrine()->getRepository(Recipe::class);
                $recipeNew = $recipeRepository->findOneById($recipe->getId());

                $recipeNew->setImageFile($oldImage);
                $recipeNew->setUser($user);
                $recipeNew->setTitle($form['title']->getData());
                $recipeNew->setIngredients($form['ingredients']->getData());
                $recipeNew->setProcess($form['process']->getData());
            } else {
                $file = $recipe->getImageFile();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move($this->getParameter('upload_directory'), $fileName);
                $recipe->setImageFile($fileName);
                $recipe->getTitle();
                $recipe->getIngredients();
                $recipe->getProcess();
            }


            if (!$recipe->getId()) {
                $recipe->setUser($user);
            }

            if ($isExist == 0) {
                $manager->persist($recipe);
            } else {
                $manager->persist($recipeNew);
            }
            $manager->flush();

            if ($isExist == 0) {
                $info = 'newRecipe';
            } else {
                $info = 'updateRecipe';
            }

            return $this->redirectToRoute('result', ['info'=>$info]);
        }

        // replace this example code with whatever you need
        return $this->render('default/createRecipe.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'isExist' => $isExist,
            'recipe' => $recipe,
            'oldTitle' => $oldTitle,
            'oldIngredients' => $oldIngredients,
            'oldProcess' => $oldProcess
        ]);

    }

    /**
     * @Route("/recettes", name="recipes_list")
     */
    public function showAllRecipeAction(Request $request)
    {
        $recipeRepository = $this->getDoctrine()->getRepository(Recipe::class);

        if (!$recipeRepository->findAll()) {
            $isNotExist = 1;
            $recipes = NULL;
        } else {
            $recipes = $recipeRepository->findAll();
            $isNotExist = NULL;
        }

        // replace this example code with whatever you need
        return $this->render('default/recipeList.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'recipes' => $recipes,
            'isNotExist' => $isNotExist
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/recette/{id}", name="recipe")
     */
    public function showRecipeAction(Request $request, $id)
    {
        $id = intval($id);
        $recipeRepository = $this->getDoctrine()->getRepository(Recipe::class);
        $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);
        if (!$recipeRepository->findOneById($id)) {
            return $this->redirectToRoute('recipes_list');
        } else {
            $recipe = $recipeRepository->findOneById($id);
            $image = $recipe->getImageFile();
            $author = $userRepository->findOneById($recipe->getUser());
        }


        // replace this example code with whatever you need
        return $this->render('default/recipe.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'recipe' => $recipe,
            'image' => $image,
            'author' => $author
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/supprimer-recette/{id}", name="delete_recipe")
     */
    public function deleteRecipeAction(Request $request, $id)
    {

        $id = intval($id);
        $user = $this->getUser();
        $recipeRepository = $this->getDoctrine()->getRepository(Recipe::class);
        $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);
        if (!$recipeRepository->findOneById($id)) {
            return $this->redirectToRoute('recipes_list');
        } else {
            $recipe = $recipeRepository->findOneById($id);
            $image = $recipe->getImageFile();
        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }

        if (!$userRepository->findOneById($recipe->getUser())) {
            return $this->redirectToRoute('recipes_list');
        } else {
            $author = $userRepository->findOneById($recipe->getUser());
        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            if ($author->getId() != $user->getId()) {
                return $this->redirectToRoute('recipes_list');
            }
        }

        // replace this example code with whatever you need
        return $this->render('default/deleteRecipe.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'recipe' => $recipe,
            'image' => $image
        ]);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/supprimer/{id}", name="delete")
     */
    public function deleteAction(Request $request, ObjectManager $manager, $id)
    {

        $id = intval($id);
        $recipeRepository = $this->getDoctrine()->getRepository(Recipe::class);
        $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);
        $user = $this->getUser();

        if (!$recipeRepository->findOneById($id)) {

            return $this->redirectToRoute('recipes_list');

        } else {

            $recipe = $recipeRepository->findOneById($id);
            $image = $recipe->getImageFile();

        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            return $this->redirectToRoute('homepage');

        }

        if (!$userRepository->findOneById($recipe->getUser())) {

            return $this->redirectToRoute('recipes_list');

        } else {

            $author = $userRepository->findOneById($recipe->getUser());

        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {

            if ($author->getId() != $user->getId()) {

                return $this->redirectToRoute('recipes_list');

            }
        }

        if ($image != NULL) {

        }

        unlink('uploads/' . $image);

        $manager->remove($recipe);
        $manager->flush();

        return $this->redirectToRoute('recipes_list');
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/modification-mot-de-passe/{info}", name="password")
     */
    public function passwordAction(Request $request, ObjectManager $manager, $info = NULL) {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        }

        $message = NULL;
        $color = NULL;

        if ($info == 'succes') {
            $message = 'Vous avez reçu un Email pour mettre a jour votre mot de passe !';
            $color = '#08cc4f';
        }
        if ($info == 'error') {
            $message = 'Il n\'y a pas de compte avec cet adresse email !';
            $color = 'red';
        }

        $email = NULL;

        $form = $this->createFormBuilder()
            ->add('email', EmailType::class)
            ->add('send', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $email = $form['email']->getData();

            $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);

            if ($user = $userRepository->findOneByEmail($email)) {

                $user->createToken();
                $user->createValidityToken();

                $manager->persist($user);
                $manager->flush();

                $info = 'succes';

                // send email
                $to = $user->getEmail();
                $subject = "nouveau mot de passe demo Eric TASCA";
                $from = "erictasca.contact@gmail.com";
                $headers = 'From: erictasca.contact@gmail.com' . "\r\n" .
                'Reply-To: erictasca.contact@gmail.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
                $message ="Cliquez sur le lien ci dessous pour créer un nouveau mot de passe :
                http://www.eric-tasca.fr/demo-recette/web/modifier-mot-de-passe/" . $user->getToken() . "
                ";
                mail($to, $subject, $message, $headers);

                return $this->redirectToRoute('password', ['info'=>$info]);

            } else {

                $info = 'error';

                return $this->redirectToRoute('password', ['info'=>$info]);

            }
        }

        // replace this example code with whatever you need
        return $this->render('default/password.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'message' => $message,
            'info' => $info,
            'color' =>$color
        ]);

    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/modifier-mot-de-passe/{token}/{info}", name="create_password")
     */
    public function createPasswordAction(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder, $token, $info = NULL) {

        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {

            return $this->redirectToRoute('homepage');

        }

        $userRepository = $this->getDoctrine()->getRepository(UserDemo::class);

        if (!$userRepository->findOneByToken($token)) {

            return $this->redirectToRoute('homepage');

        } else {

            $user =$userRepository->findOneByToken($token);

        }

        $date = new DateTime();

        if ($date > $user->getValidityToken()) {

            return $this->redirectToRoute('password');

        }

        $message = NULL;
        $color = NULL;

        if ($info == 'succes') {
            $message = 'Mot de passe modifié !';
            $color = '#08cc4f';
        }

        if ($info == 'error') {

            $message = 'Les deux mot de passe doivent être identiques !';
            $color = 'red';

        }

        $password = NULL;

        $form = $this->createFormBuilder()
            ->add('password', PasswordType::class)
            ->add('confirm_password', PasswordType::class)
            ->add('send', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $password = $form['password']->getData();
            $confirmPassword = $form['confirm_password']->getData();

            if ($password != $confirmPassword) {

                $info = 'error';

                return $this->redirectToRoute('create_password', ['token'=>$token, 'info'=>$info]);

            } else {

                $info = 'change_pass';

                $hash = $encoder->encodePassword($user, $password);

                $user->setPassword($hash);
                $user->setToken(NULL);
                $user->setValidityToken(NULL);

                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute('result', ['info'=>$info]);

            }
        }

        return $this->render('default/createPassword.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'token' => $token,
            'info' => $info,
            'message' => $message,
            'color' => $color
        ]);

    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/message-validation/{info}", name="result")
     */
    public function validAction($info = NULL) {

        $title = NULL;
        $message = NULL;

        if ($info == 'change_pass') {
            $title = 'Modification du mot de passe';
            $message = 'Action réussie, votre changement a été pris en compte';
        }

        if ($info == 'newRecipe') {
            $title = 'Action réussie';
            $message = 'Vous avez ajouté une nouvelle recette !';
        }

        if ($info =='updateRecipe') {
            $title = 'Action réussie';
            $message = 'Vous avez modifié votre recette !';
        }

        return $this->render('default/result.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'title' => $title,
            'message' => $message
        ]);
    }

}
